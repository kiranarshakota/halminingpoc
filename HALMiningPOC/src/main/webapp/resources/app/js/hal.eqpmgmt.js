HAL.eqpmgmt = {

init: function(){
	console.log('HAL.eqpmgmt');
	HAL.eqpmgmt.bindEvents();
	HAL.eqpmgmt.initiateWidgets();

},

bindEvents: function(){
	console.log('bindEvents');
	$('.eqp-vehicle').on('click', HAL.eqpmgmt.onEqpVehicleClick);
},

initiateWidgets: function(){

	// Initiate Tracker's Accordion
	$('#accordion').accordion();

	// Iniatiate and Draw the chart
	HAL.charts.draw(HAL.charts.data);

	// Initiate Footer's Ticker
	$('.ticker').easyTicker({
		direction: 'up',
		easing: 'swing',
		speed: 'slow',
		interval: 4000,
		height: 'auto',
		visible: 1,
		mousePause: 1,
	});

},

onEqpVehicleClick: function(e){
	console.log('onEqpVehicleClick', this, e);
}

};