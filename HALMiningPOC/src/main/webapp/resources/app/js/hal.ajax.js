HAL.ajax = {

	httpErrors: {
		'0': 'Connection',
		'1': 'Response not expected',
		'2': 'Authentication',
		'3': 'Redirectional',
		'4': 'Bad Request',
		'5': 'Server'
	},

	call: function(service, data, sCallback, eCallback, isGet){

        //console.log('Ajax module initiated!!');
        var params = {

        	url: HAL.su,
        	data: data,
            contentType: "text/xml", 
            dataType: 'xml',
            type: isGet ? 'GET': 'POST',

            beforeSend: function(){

            	// Check Connection before any AJAX Call
            	PG.checkConnection();
				if(!PG.connected){
					HAL.leapApp.showError('NO NETWORK', 'Please connect to Internet!');
					return false;
				}

				// Start showing loader.
				HAL.leapApp.showLoader();

            },
		  	
		  	success: function(result, status, xhr){
            	//console.log('SOAP Success');
            	if(result && result != ''){
	            	var json = $.xml2json(result);
	            	var eObj = json.Body;
	            	//console.log('eObj: ', eObj);

			  		if(sCallback){
			  			sCallback(eObj, status, xhr);
			  		}else{
			  			return [result, status, xhr];
			  		}
            	}else{
            		HAL.ajax._showError('INVALID RESPONSE', 'Blank Response');
            	}
		  	},
		  	
		  	error: function(xhr, status, error){
		  		//console.log(error);
				if(eCallback){
					eCallback(xhr, status, error);
				}else{
					HAL.ajax._showError(xhr, status, error);
					return [xhr, status, error];
				}
		  	},

		  	complete: function(){
				// Hide loader.
				HAL.leapApp.hideLoader();
		  	}
        };

		if(service && service != ''){
	        if(data){
	            params.data = HAL.ajax._constructXML(service, data);
	        }else{
	        	params.data = {};
	        }	
		}else{HAL.ajax._showError('INVALID SERVICE NAME', 'Please call the right service.');}
		
		$.ajax(params);
		
	},

	_constructXML: function(service, data){
        var prefixRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:HAL="http://HAL.com/"><soapenv:Header/><soapenv:Body>';
        var postfixRequest = '</soapenv:Body></soapenv:Envelope>';
        var prefixService = '<HAL:' + service + '>';
        var postfixService = '</HAL:' + service + '>';
        var xmlData = HAL.util.json2xml(data);

		//console.log(arguments);
		var resp =  prefixRequest + prefixService + xmlData + postfixService + postfixRequest;
		console.log(resp);
		return resp;
	},

	_pickError: function(errCode){

		var errInt = parseInt(errCode/100);
		
		var errMsg = this.httpErrors[errInt];
		return errMsg;

	},

	_showError: function(xhr, status, error){
		//alert(desc);
		console.log(xhr, status, error);
		var cpg = $.mobile.activePage;
		var errObj = $("<div>");


		//console.log(cpg);
		var errBtn = $('<div>')
						//.attr('href', '#')
						.attr('data-role','button')
						.attr('data-icon','delete')
						.attr('data-iconpos','notext')
						.addClass('ui-btn-right')
						.html('Close')
						.appendTo(errObj);
		errBtn.button();
		errBtn.on('click', function(){
			errObj
				.popup('close')
				.remove();

		});

		var errObjHeader = $("<h4>")
							.appendTo(errObj);
		var errObjDesc = $("<div>")
							.appendTo(errObj);
		errObj.css('padding', '15px');
		//errObjHeader.html(xhr.status +' '+xhr.statusText);
		errObjHeader.html(HAL.ajax._pickError(xhr.status) +' error');
		//errObjDesc.html(xhr.responseText);
		errObjDesc.html('An error occured.');
		errObj.appendTo(cpg);
		errObj
			.popup()
			.popup("open");
	}
};

/*
$(document).ajaxStart(function() {
	console.log('ajaxStart');
	HAL.leapApp.showLoader();
})

$(document).ajaxComplete(function() {
	console.log('ajaxComplete');
	HAL.leapApp.hideLoader();
})
    
$(document).ajaxStop(function() {
	console.log('ajaxStop');
});
*/