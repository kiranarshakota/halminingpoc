HAL.charts = {

  //add as many elements to data as necessary, all layers should be present in first data element, add or remove layer elements as necessary
  data: [
      {
       id: 1,
       name: '1',
        layers: {a: 10, b: 13, c: 12, d: 22}
      }, 
      {
       id: 2,
       name: '2',
        layers: {a: 8, b: 5, c: 14, d:15}
      },
      {
       id: 3,
       name: '3',
        layers: {a: 3, b: 5, c: 7, d:10}
      },
      {
       id: 4,
       name: '4',
        layers: {a: 4, b: 15, c: 14, d:5}
      }
  ],

  draw: function(data) {
  //var data = HAL.charts.data;
  //get the layernames present in the first data element
  var layernames = d3.keys(data[0].layers);
  
  //get id heights, to use for determining scale extent, also get barnames for scale definition
  var idheights = [];
  var barnames = [];
  for (var i=0; i<data.length; i++) {
    tempvalues = d3.values(data[i].layers);
    tempsum = 0;
    for (var j=0; j<tempvalues.length; j++) {
    tempsum = tempsum + tempvalues[i];
    }
    idheights.push(tempsum);
    barnames.push(data[i].name);
  };

  var margin = {top: 10, right: 10, bottom: 30, left: 30},
    width = 400 - margin.left - margin.right,
    height = 300 - margin.top - margin.bottom;

  var x = d3.scale.ordinal()
    .domain(barnames)
    .rangeRoundBands([0, width], .25);

  var y = d3.scale.linear()
    .domain([0, d3.max(idheights)])
    .range([height, 0]);
    
  var colors = d3.scale.category20();

  var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

  var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

  var svg = d3.select("#chart").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  svg.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  svg.append("g")
    .attr("class", "y axis")
    .call(yAxis);
    
  //add a g element for each bar
  var bargroups = svg.append("g")
    //.attr("class", "bars")
    .selectAll("g")
    .data(data, function(d) {return d.id;})
    .enter().append("g")
    .attr("id", function(d) {return d.name;});
    
  //sub-selection for rect elements
  var barrects = bargroups.selectAll("rect")
    .data(function(d) {
      //set data as an array of objects [{height: _, y0: _},..]
      //must compute sum of other elements to get y0 (computed height)
      var temparray = [];
      var tempsum = 0;
      for (var i=0; i<layernames.length; i++) {
        console.log(layernames[i]);
        temparray.push(
          {height: d.layers[layernames[i]],
           y0: tempsum + d.layers[layernames[i]]
           }
        );
        tempsum = tempsum + d.layers[layernames[i]];
      }
      return temparray;
    })
    .enter().append("rect")
    .attr({
      "x": function(d,i,j) {return x(barnames[j]);},
      "y": function(d) {return y(d.y0);},
      "width": x.rangeBand(),
      "height": function(d) {return height - y(d.height);}
    })
    .style("fill", function(d,i,j) {return colors(i)});   
    
    var legend = svg.selectAll(".legend")
    .data(colors.domain().slice().reverse())
    .enter().append("g")
    .attr("class","legend")
    .attr("transform", function(d,i){
      return "translate(0,"+i*20+")";
    }); 
    
    legend.append("rect")
    .attr("x", width - 5)
    .attr("width", 18)
    .attr("height", 18)
    .attr("fill", colors);
    
    legend.append("text")
    .attr("x", width-10)
    .attr("y",9)
    .attr("dy",".35em")
    .style("text-anchor","end")
    .text(function(d){return d;});
    
    // Event handler of buttons
    d3.selectAll("button").on('click', function(){
      // exit selection
      d3.selectAll("svg").remove();
      
      var number_of_data_points = this.getAttribute("id");
      if (number_of_data_points == "KPI1") {
        HAL.charts.update1();
      } else if (number_of_data_points == "KPI2") {
        HAL.charts.update2();
      }else if (number_of_data_points == "KPI3") {
        HAL.charts.update3();
      }
    });
    
  },

  update1: function() {
  
    console.log("update1");
    var data = [
      {
      id: 1,
      name: '1',
      layers: {a: 8, b: 5, c: 14, d:15}
        
      }, 
      {
      id: 2,
      name: '2',
      layers: {a: 10, b: 13, c: 12, d: 22}
      },
      {
      id: 3,
      name: '3',
      layers: {a: 4, b: 15, c: 14, d:5}
      },
      {
      id: 4,
      name: '4',
      layers: {a: 3, b: 5, c: 7, d:10}
        
      }
    ]
    
    HAL.charts.draw(data);
  },

  update2: function() {
  
    console.log("update2");
    var data = [
      {
      id: 1,
      name: '1',
      layers: {a: 4, b: 15, c: 14, d:5}
      }, 
      {
      id: 2,
      name: '2',
      layers: {a: 10, b: 13, c: 12, d: 22}
      },
      {
      id: 3,
      name: '3',
      layers: {a: 3, b: 5, c: 7, d:10}    
      },
      {
      id: 4,
      name: '4',
      layers: {a: 8, b: 5, c: 14, d:15}
        
      }
    ]
    
    HAL.charts.draw(data);
  },
  
  update3: function() {
  
    console.log("update3");
    var data = [
      {
      id: 1,
      name: '1',
      layers: {a: 8, b: 5, c: 14, d:15}
        
      }, 
      {
      id: 2,
      name: '2',
      layers: {a: 10, b: 13, c: 12, d: 22}
      },
      {
      id: 3,
      name: '3',
      layers: {a: 4, b: 15, c: 14, d:5}
      },
      {
      id: 4,
      name: '4',
      layers: {a: 3, b: 5, c: 7, d:10}
        
      }
    ]

    HAL.charts.draw(data);
  }

};