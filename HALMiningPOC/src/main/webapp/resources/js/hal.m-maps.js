
(function ($) {
    $(function () {
        /* initialize watch_id for geolocation poll handler. May be useful in the future */
        var watch_id;
        var zoom_level = 17;
        var map_dom_element = $("#map_container").get(0);
        var map_options = {
            center: new google.maps.LatLng(36.17004623489614, -115.1457341421891),
            maxZoom: 19,
            minZoom: 18,
            zoom: zoom_level,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            /* TODO must disable other types of zooming as well! */
            zoomControl: false,
            mapTypeControl: false,
            panControl: false,
            overviewMapControl: false,
            streetViewControl: false
        }
        var map = new google.maps.Map(map_dom_element, map_options);
        var overlay_image = new google.maps.MarkerImage('app/images/background/landrys_map_downstairs_smaller.png',
                                                        new google.maps.Size(608, 580),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(147, 568));

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(36.16882068583446, -115.14655489814766),
            map: map,
            icon: overlay_image,
            title: "Golden Nugget",
            visible: true
        });

        var meeting_image = new google.maps.MarkerImage('app/images/background/map_upper_overlay.png',
                                                        new google.maps.Size(608, 581),
                                                        new google.maps.Point(0, 0),
                                                        new google.maps.Point(98, 569));

        var marker_3 = new google.maps.Marker({
            position: new google.maps.LatLng(36.168654, -115.146032),
            map: map,
            icon: meeting_image,
            title: "Golden Nugget",
            visible: false
        });

        var marker_2 = new google.maps.Marker({
            position: new google.maps.LatLng(31.578188, -84.154139),
            map: map,
            title: "Where I Am",
            zIndex: 999
        });

        var geo_success = function (position){
            console.log('Map Success');
            map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
            marker_2.setPosition(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
        }

        var geo_error = function (err) {
            if (err.code == 1) {
                $("#permission-denied-message").show();
            } else {
                $("#general-error-message").show();
            }
        }

        $("#get_current_location_desktop").click(function (e) {
            if (geoPosition.init()) {
                geoPosition.getCurrentPosition(geo_success, geo_error, { maximumAge: 75000, enableHighAccuracy: true });
            }
            e.preventDefault();
        });

        $("#get_current_location").click(function (e) {
            if (geoPosition.init()) {
                geoPosition.getCurrentPosition(geo_success, geo_error, { maximumAge: 75000, enableHighAccuracy: true });
            }
            e.preventDefault();
        });
		
        $("li[class*='_tab'] img").click(function () {
            if ($(this).attr('src').indexOf("_on") > -1) {
                //An active tab was clicked so do nothing
            } else {
                $(this).attr('src', $(this).attr('src').replace("_off", "_on", "gi"));
                var inactive_element = $(this).parent('li').siblings("li[class*='_tab']").children("img");
                inactive_element.attr('src', $(inactive_element).attr('src').replace("_on", "_off", "gi"));
                if ($(this).parent("li[class*='_tab']").attr('class') == 'ff_tab') {
                    marker.setVisible(true);
                    marker_3.setVisible(false);
                    zoom_level = 17;
                    map.panTo(new google.maps.LatLng(36.17004623489614, -115.1457341421891));
                    map.setZoom(zoom_level);
                } else if ($(this).parent("li[class*='_tab']").attr('class') == 'mr_tab') {
                    marker.setVisible(false);
                    marker_3.setVisible(true);
                    zoom_level = 18;
                    map.setZoom(zoom_level);
                    map.panTo(new google.maps.LatLng(36.169252917799195, -115.14586288822181));
                }
            }
        });
		
		
		/*
        $("a[class*='_tab'] img").click(function () {
            if ($(this).attr('src').indexOf("_on") > -1) {
                //An active tab was clicked so do nothing
            } else {
                $(this).attr('src', $(this).attr('src').replace("_off", "_on", "gi"));
                var inactive_element = $(this).parent('a').siblings("a[class*='_tab']").children("img");
                inactive_element.attr('src', $(inactive_element).attr('src').replace("_on", "_off", "gi"));
                if ($(this).parent("a[class*='_tab']").attr('class') == 'ff_tab') {
                    marker.setVisible(true);
                    marker_3.setVisible(false);
                    zoom_level = 18;
                    map.panTo(new google.maps.LatLng(36.17004623489614, -115.1457341421891));
                    map.setZoom(zoom_level);
                } else if ($(this).parent("a[class*='_tab']").attr('class') == 'mr_tab') {
                    marker.setVisible(false);
                    marker_3.setVisible(true);
                    zoom_level = 19;
                    map.setZoom(zoom_level);
                    map.panTo(new google.maps.LatLng(36.169252917799195, -115.14586288822181));
                }
            }
        });
		*/
    });
})(jQuery);