var HAL = HAL || {};

// HAL framework level urls..
HAL.fu = 'app/'; // front end url
HAL.su = 'http://landrysleap.com/LeadershipConferenceServices.asmx'; // service url
HAL.du = 'app/data/'; // data url
//HAL.ru = 'http://www.landrysleap.com/sponsors/resources/logos/'; // resource url
//HAL.ru = 'https://farm8.staticflickr.com/'; 

HAL.ru = 'app/images/resources/sponsors/logos/'; // resource local url
HAL.lu = 'http://www.landrysleap.com/images/mobile/logos/'; // logos url
HAL.tu = 'http://www.landrysleap.com/images/mobile/logos/thumbnails/'; // thumbnail url

// HAL mode
HAL.mode = 'd'; // p: production, d: development, t: testing

HAL.init = function(){
 	//console.log('HAL.init');
 	HAL.eqpmgmt.init();
}
