HAL.storage = {

	html5StorageEnabled: (window['localStorage'] && window['sessionStorage']),
	appStoragePrefix: 'HAL_',
    
    set: function(name, value, isForSession){
        if(HAL.storage.html5StorageEnabled){
            if(isForSession){
                sessionStorage.setItem(HAL.storage.appStoragePrefix+name, value);
            }else{
                localStorage.setItem(HAL.storage.appStoragePrefix+name, value);
            }
        }
    },

    setObject: function(name, value, isForSession){
        if(HAL.storage.html5StorageEnabled){
            if(isForSession){
                sessionStorage.setItem(HAL.storage.appStoragePrefix+name, JSON.stringify(value));
            }else{
                localStorage.setItem(HAL.storage.appStoragePrefix+name, JSON.stringify(value));
            }
        }
    },

    get: function(name){
        if(HAL.storage.html5StorageEnabled){
            var sessionValue = sessionStorage.getItem(HAL.storage.appStoragePrefix+name);
            if(sessionValue != null){
                return sessionValue;
            }
            return localStorage.getItem(HAL.storage.appStoragePrefix+name);
        }
    },

    getObject: function(name){
        if(HAL.storage.html5StorageEnabled){
            var sessionValue = sessionStorage.getItem(HAL.storage.appStoragePrefix+name);
            if(sessionValue != null){
                return JSON.parse(sessionValue);
            }
            return JSON.parse(localStorage.getItem(HAL.storage.appStoragePrefix+name));
        }
    },

    remove: function(name){
        if(HAL.storage.html5StorageEnabled){
            sessionStorage.removeItem(HAL.storage.appStoragePrefix+name);
            localStorage.removeItem(HAL.storage.appStoragePrefix+name);
        }
    },

    show: function(){
		for (var i = 0; i < localStorage.length; i++){
			var key = localStorage.key(i);
			var value = localStorage[key];
			if(key.search(HAL.storage.appStoragePrefix) != -1){
				console.log(key, ' : ' , value);
			}
		}
		return;
    },

    flush: function(){
		for (var i = 0; i < localStorage.length; i++){
			var key = localStorage.key(i);
			if(key.search(HAL.storage.appStoragePrefix) != -1){
				localStorage.removeItem(key);
				console.log(key+' removed');
			}
		}
		return;
    }

};