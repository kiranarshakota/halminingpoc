<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HAL - Equipment Management</title>
<link rel="stylesheet" href="resources/css/hal.css" />
<link rel="stylesheet" href="resources/css/themes/base/jquery.ui.all.css"></head>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>
<script>
	function initialize()
	{
	var mapProp = {
	  center:new google.maps.LatLng(33.961708, -118.336696),
	  zoom:15,
	  mapTypeId:google.maps.MapTypeId.ROADMAP
	  };
	var map=new google.maps.Map(document.getElementById("GMAPS")
	  ,mapProp);
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<body>
<form name="frmEqp" id="frmEqp">  

<div id="CONTAINER">
	<div class="header"><h3>portal navigation</h3></div>
	<div id="GMAPS">

	</div>
	<div class="DETAIL">
		<div class="tracker"><h4>real - time event tracker</h4>

<div id="accordion">
  <h3>RT View</h3>
  <div>
    <p>
    <img class="eqp-vehicle" src="resources/images/t1.png" />
    <img class="eqp-vehicle" src="resources/images/t2.png" />
    <img class="eqp-vehicle" src="resources/images/t3.png" />
    <img class="eqp-vehicle" src="resources/images/t4.png" />
    </p>
  </div>
  <h3>Event Details</h3>
  <div>
    <p>
    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet
    purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor
    velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In
    suscipit faucibus urna.
    </p>
  </div>
  <h3>Weather</h3>
  <div>
    <p>
    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis.
    Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero
    ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis
    lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
    </p>
  </div>
 
</div>

		</div>
		<div class="charts">
			<h4>operation status - key performance indicator</h4>
			<div>
				<button id="KPI1">KPI1</button>
				<button id="KPI2">KPI2</button>
				<button id="KPI3">KPI3</button>
			</div></br>
			<div id="chart"></div>
		</div>
	</div>
	<div class="ticker">
		<ul>
			<li>Alert: Truck #WC153 is late by 63 minutes and is moving to site now.</li>
			<li>Message: All newly hired Trucks from L&T are health checked and reported fine. </li>
			<li>Alert: Rain delayed the loading of 'L-Series' Trucks.</li>
			<li>Message: Site no. 2 is now equipped with special security.</li>
		</ul>
	</div>
</div>

<script src="resources/lib/js/jquery-1.11.0.js"></script>
<script src="resources/lib/js/jquery.ui.core.js"></script>
<script src="resources/lib/js/jquery.ui.widget.js"></script>
<script src="resources/lib/js/jquery.ui.accordion.js"></script>
<script src="resources/lib/js/jquery.easing.min.js"></script>
<script src="resources/lib/js/jquery.easy-ticker.js"></script>
<script src="resources/lib/js/d3.js" charset='utf-8'></script>

<script src="resources/app/js/hal.js"></script>
<script src="resources/app/js/hal.ajax.js"></script>
<script src="resources/app/js/hal.util.js"></script>
<script src="resources/app/js/hal.storage.js"></script>
<script src="resources/app/js/hal.eqpmgmt.js"></script>
<script src="resources/app/js/hal.charts.js"></script>

<script>$(function(){ HAL.init(); });</script>

</form>
</body>
</html>